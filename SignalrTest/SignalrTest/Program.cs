﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalrTest
{
    class Program
    {
        static void Main(string[] args)
        {
            HubConnection connection;

            connection = new HubConnectionBuilder()
               .WithUrl("http://localhost:55124/chat")
               .Build();

            /*var connection = new HubConnection("http://localhost:55124/chat");
            //Make proxy to hub based on hub name on server
            var myHub = connection.CreateHubProxy("ChatHub");*/
            //Start connection

            connection.StartAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine("There was an error opening the connection:{0}",
                                      task.Exception.GetBaseException());
                }
                else
                {
                    Console.WriteLine("Connected");
                }

            }).Wait();
            Console.ReadKey();
            connection.InvokeAsync<string>("SendMessageToUser", "HELLO World ").ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine("There was an error calling send: {0}",
                                      task.Exception.GetBaseException());
                }
                else
                {
                    Console.WriteLine(task.Result);
                }
            });
            Console.WriteLine("Waiting for message to come");
            connection.On<string>("ChatUpdate", param =>
            {
                Console.WriteLine(param);
            });


            Console.Read();
            string alma = Console.ReadLine();
            connection.InvokeAsync<string>("SendMessageToUser", alma).ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine("There was an error calling send: {0}",
                                      task.Exception.GetBaseException());
                }
                else
                {
                    Console.WriteLine(task.Result);
                }
            });
            connection.StopAsync();
        }
    }
}


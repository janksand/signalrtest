﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalrTest.Server
{
    public class ChatHub : Hub
    {
        [HubMethodName("SendMessageToUser")]
        public async Task SendMessage(string message)
        {
            await Clients.All.SendAsync("ChatUpdate", message);
        }
    }
}
